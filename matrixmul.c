#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <assert.h>
#include <getopt.h>
#include <stddef.h>

#include "densematgen.h"

#define SPARSE_SEND_DIMENSIONS 1
#define SPARSE_SEND_ELEMENTS 2
#define SPARSE_SEND_IA 3
#define SPARSE_SEND_JA 4

#define SHIFT_ELEMENTS 20
#define SHIFT_JA 30
#define SHIFT_IA 40

#define SPARSE_GATHER_IA 5

#define COL_A_SPARSE_SEND_IA 10
#define COL_A_SPARSE_SEND_JA 11
#define COL_A_SPARSE_SEND_ELEMENTS 12

#define min(X, Y)  ((X) < (Y) ? (X) : (Y))
#define max(X, Y)  ((X) > (Y) ? (X) : (Y))


typedef struct{
	int rows;
	int columns;
	int N;
	int NNZ;
	int maxNonZeroPerRow;
	double * elements;
	int * IA_row_offsets;
	int * JA_column_indices;
}* sparse_type, sparse_ref;

int modulo(int a, int mod){
		int res = a % mod;
		if(res < 0)
			return mod + res;
		return res;

}

/*************************HELPER FUNCTIONS*******************************************/
void printSparse(sparse_type sparse){
printf("sparse %d %d last %d first %d\n", sparse->rows, sparse->N,sparse->IA_row_offsets[sparse->rows],sparse->IA_row_offsets[0]);
int elems = sparse->IA_row_offsets[sparse->rows]-sparse->IA_row_offsets[0];
for(int i = 0; i < elems; ++i)
	printf("%lf ", sparse->elements[i]);
	printf("\n");
	
for(int i = 0; i <= sparse->rows; ++i)
	printf("%d ", sparse->IA_row_offsets[i]);
	printf("\n");
for(int i = 0; i < elems; ++i)
	printf("%d ", sparse->JA_column_indices[i]);
	printf("\n");
}

int ile_grup(int num_processes, int repl_fact){
	return num_processes / repl_fact;
}
	
int my_group_number(int mpi_rank_comm_world, int repl_fact){
	return mpi_rank_comm_world / repl_fact;
}

int ile_iteracji(int num_processes, int repl_fact){
	return (num_processes / repl_fact )/ repl_fact + ((num_processes / repl_fact ) % repl_fact > 0?1:0);
}

int diagonal_group_num(int mpi_rank_comm_world, int rank_in_group, int repl_fact, int num_processes){
	return modulo((my_group_number(mpi_rank_comm_world,repl_fact) - (repl_fact - rank_in_group - 1) * ile_iteracji(num_processes,repl_fact)) , ile_grup( num_processes, repl_fact));
}

int my_rank_in_diagonal_group(int my_rank_comm_world, int repl_fact){
	return my_rank_comm_world % repl_fact;
}

int my_rank_in_group(int my_rank_comm_world, int repl_fact){
	return modulo(my_rank_comm_world  , repl_fact);
}

int prawy_sasiad(int mpi_rank_comm_world, int repl_fact, int num_processes){
	return modulo((mpi_rank_comm_world + repl_fact),num_processes);
}

int lewy_sasiad(int mpi_rank_comm_world, int repl_fact, int num_processes){
	return modulo(mpi_rank_comm_world - repl_fact,num_processes);
}

int prawy_sasiad_koniec_rundy(int mpi_rank_comm_world, int repl_fact, int num_processes){
	return (mpi_rank_comm_world + repl_fact * (ile_iteracji(num_processes, repl_fact) - 1)) %num_processes;
}

int lewy_sasiad_koniec_rundy(int mpi_rank_comm_world, int repl_fact, int num_processes){
	return modulo(mpi_rank_comm_world - repl_fact*(ile_iteracji(num_processes, repl_fact) - 1),num_processes);
}

int myFirst(int columns, int rank, int  num_processes) {
	int colSize = columns / num_processes; 
	int restCols = columns % num_processes;
	return rank * colSize + min(rank, restCols);
}

int myLast(int columns, int rank,int  num_processes) {
	return myFirst(columns, rank + 1, num_processes) - 1;
}
/********************************************************************/


/*************************COMMON FUNCTIONS*******************************************/
 double* generate_dense_inner(int rows, int columns, int startRow, int startColumn, int gen_seed, int originalN){
	double * result = malloc(sizeof(double) * rows * columns); ///sparse->columns / p ? wypisanie macierzy kolumnami
	int next = 0;
	
	for(int wiersz = startRow; wiersz < startRow + rows; ++wiersz)
		for(int kolumna = startColumn; kolumna < startColumn + columns; ++kolumna)
		{
			if(kolumna < originalN && wiersz < originalN){
					result[next++] = generate_double(gen_seed, wiersz,kolumna);
			}
			else
				result[next++] = 0.0;
		}
		//printf("next gen rank %d %d\n", startColumn, next);
	return result;
 }
 
  /********************************************************************/

 /*****************************FUNCTIONS FOR Col A*********************************************************************/
 
 
 /*****************************Main Col A function******************************************************************/

 double* colA(sparse_type sparse, int num_processes, int repl_fact, int mpi_rank, double *B, int exponent){
	double * C = malloc(sizeof(double) * sparse->N * (sparse->rows / repl_fact));
	MPI_Request IARequest, JARequest, ElementsRequest;
	MPI_Status myStatus;
	int * toFreeIA = NULL;
	int * toFreeJa= NULL;
	double * toFreeElems = NULL;

	int currentCol = 0, seenElements;
	//offset = nr grupy * rows
	int offset = (mpi_rank / repl_fact) * sparse->rows;
	
	for(int expIter = 0; expIter < exponent; ++ expIter){
	
		for(int numElem = 0; numElem < (sparse-> rows / repl_fact) * sparse->N; ++numElem){
			C[numElem] = 0;
		}
		
		for(int round = 0; round < num_processes / repl_fact; ++round)
		{
			seenElements = sparse->IA_row_offsets[0];
			currentCol = 0;
			for(int numElem = 0; numElem < sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0]; ++numElem){
					while(seenElements >= sparse->IA_row_offsets[currentCol + 1])
							currentCol++;
		
					int resultRow = (sparse->JA_column_indices[numElem]);			
								
					for(int internalColumn = 0; internalColumn < sparse->rows / repl_fact; ++internalColumn){
						C[(sparse->rows/ repl_fact) * resultRow + internalColumn]  
						+= B[(currentCol + offset)* (sparse->rows/ repl_fact) + internalColumn] * sparse->elements[numElem];
					}
					seenElements++;
			}
			offset +=  sparse->rows;
			offset %= sparse->N;

			//przesuń dane o jeden w lewo
			
			if(toFreeIA != NULL){
				MPI_Wait(&IARequest, &myStatus);
				free(toFreeIA);
				toFreeIA = NULL;
				MPI_Wait(&ElementsRequest, &myStatus);
				free(toFreeElems);
				toFreeElems = NULL;
				MPI_Wait(&JARequest, &myStatus);
				free(toFreeJa);
				toFreeJa = NULL;
			}
			
			MPI_Isend(
				sparse->IA_row_offsets,
				sparse->rows + 1,
				MPI_INT, 
				modulo(mpi_rank - repl_fact, num_processes),  
				SHIFT_IA,
				MPI_COMM_WORLD, 
				&IARequest);

			MPI_Isend(
				sparse->elements,
				sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
				MPI_DOUBLE, 
				modulo(mpi_rank - repl_fact, num_processes),
				SHIFT_ELEMENTS,
				MPI_COMM_WORLD, 
				&ElementsRequest);

			MPI_Isend(
				sparse->JA_column_indices,
				sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
				MPI_INT, 
				modulo(mpi_rank - repl_fact, num_processes),
				SHIFT_JA,
				MPI_COMM_WORLD, 
				&JARequest);
			
			int* buf = malloc(sizeof(int) * sparse->rows +1);
			
			MPI_Recv(
				buf,
				sparse->rows + 1,
				MPI_INT,
				modulo(mpi_rank + repl_fact, num_processes),
				SHIFT_IA,
				MPI_COMM_WORLD,
				&myStatus);
					
			//przepinam bufor, który może jeszze się wysyła asynchronicznie na zmienną do usunięcia w kolejnym obrocie
			toFreeIA = sparse->IA_row_offsets;
			sparse->IA_row_offsets = buf;
			
			int *JAbuf = malloc(sizeof(int) * sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0]);
			double* tmpElements =  malloc(sizeof(double) * sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0]);
			
			MPI_Recv(
				tmpElements,
				sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
				MPI_DOUBLE,
				modulo(mpi_rank + repl_fact, num_processes),
				SHIFT_ELEMENTS,
				MPI_COMM_WORLD,
				&myStatus);
				
			toFreeElems = sparse->elements;
			sparse->elements = tmpElements;
				
			MPI_Recv(
				JAbuf,
				sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
				MPI_INT,
				modulo(mpi_rank + repl_fact, num_processes),
				SHIFT_JA,
				MPI_COMM_WORLD,
				&myStatus);
				
			toFreeJa = sparse-> JA_column_indices;
			sparse-> JA_column_indices = JAbuf;			
		}
		
		free(B);
		B = C;
		C =  malloc(sizeof(double) * sparse->N * sparse->rows);
	}
	
	return B;
 }
 
  /****************************cache dense*******************************************************************/
 double* generate_dense(int rows, int columns,  int startColumn, int gen_seed, int originalN){
	return generate_dense_inner(rows, columns, 0, startColumn, gen_seed,  originalN);
 }
 
  /****************************cache dense*******************************************************************/
 double* cache_dense(sparse_type sparse, int gen_seed, int repl_fact, int mpi_rank, int num_processes, MPI_Comm *group_comm, int originalN){
	int first = myFirst(sparse->N, mpi_rank, num_processes);

	return  generate_dense(sparse->N, (sparse->N ) / num_processes, first, gen_seed, originalN);
 }
 
 /*****************************Gather sparse********************************************************************/
  void gather_sparse_Col_A(sparse_type sparse, int mpi_rank, int num_processes, int repl_fact, MPI_Comm *group_comm, int * rank_in_group){
 
	if(repl_fact > 1){ //c==1 -> kazdy ma swoje elementy
		int nr_grupy = mpi_rank / repl_fact;
		//tworzenie komunikatora na potrzeby grupy
		
		MPI_Comm_split(
            MPI_COMM_WORLD,
            nr_grupy,
            my_rank_in_group(mpi_rank, repl_fact),
            group_comm);
	
		int group_size;
		MPI_Comm_size(*group_comm, &group_size);
        MPI_Comm_rank(*group_comm, rank_in_group);
		
		//gather - zbierz dane z grupy do przedstawiciela - rank n-1
		int rowsToSend = myLast(sparse->N, mpi_rank, num_processes) - myFirst(sparse->N, mpi_rank, num_processes) + 1;

		//wszyscy zbierają dane o macierzy, w danej grupie w danej rundzie będzie ten sam "duży pasek" A
		int * resbuf = malloc(sizeof(int) * rowsToSend * group_size + 1);  //+1 na potrzeby ostatniego elementu IA
		int * iaElements = NULL, *iaDispls = NULL;
		//policzenie NNz per proces do GatherV
		iaElements = malloc(sizeof(int)* group_size);
		iaDispls = malloc(sizeof(int)* group_size);
		
		for(int i = 0; i < group_size; ++i){
			iaElements[i] = rowsToSend;
			iaDispls[i] = i* rowsToSend;
		}
		iaElements[group_size - 1]++; //na N-1-szy element macirzy offsetów

	   //zebranie IA, w tym kontekście IA column offsets
	   MPI_Allgatherv(
				sparse->IA_row_offsets, 
				iaElements[*rank_in_group],
				MPI_INT,
				resbuf,
				iaElements,
				iaDispls, 
				MPI_INT, 
				*group_comm);

		free(sparse->IA_row_offsets);
		sparse->IA_row_offsets = resbuf;
	
		//wszyscy mają IA dla całej diagonalnej grupy
		int * nnzs = NULL, *displs = NULL, *JAelements = NULL;
		//policzenie NNz per proces do GatherV

		double * elementsBuf = NULL;;
		
		nnzs = malloc(sizeof(int)* group_size);
		displs = malloc(sizeof(int)* group_size);
		nnzs[0] = sparse->IA_row_offsets[rowsToSend] - sparse->IA_row_offsets[0];
		displs[0] = 0;
		for(int i = 1; i < group_size; ++i){
			nnzs[i] = sparse->IA_row_offsets[(i+1) * rowsToSend] - sparse->IA_row_offsets[i * rowsToSend];
			displs[i] = displs[i-1] + nnzs[i -1];	
		}
		//bufor na wszystkie elementy nnz z danego paska wierszy macierzy A
		elementsBuf = malloc(sizeof(double) * (sparse->IA_row_offsets[group_size * rowsToSend] - sparse->IA_row_offsets[0]));
		
		//zebranie elements
			
		MPI_Allgatherv(
				sparse->elements, 
				nnzs[*rank_in_group], 
				MPI_DOUBLE,
				elementsBuf,
				nnzs,  
				displs,
				MPI_DOUBLE, 
				*group_comm);
		
		//przepisanie wszystkich elementów do bufora 
		free(sparse->elements);
		sparse->elements = elementsBuf;
		JAelements = malloc(sizeof(int) * sparse->IA_row_offsets[group_size * rowsToSend] - sparse->IA_row_offsets[0]);
	
		//zebranie JA
		MPI_Allgatherv(
				sparse->JA_column_indices,
				nnzs[*rank_in_group], 
				MPI_INT,
				JAelements,
				nnzs,  
				displs,
				MPI_INT, 
				*group_comm);
	
		//przepięcie bufora na JA kierownika
		
		free(sparse->JA_column_indices);
		sparse->JA_column_indices = JAelements;
	}
	else
	 (*rank_in_group) = 0;
 }
 
 /*******************************cache sparse col a*****************************************************/
 
 void cache_sparse(sparse_type sparse, int mpi_rank, int num_processes, int repl_fact , MPI_Comm *group_comm, int * rank_in_group){
		//alokacja pamięci na IA: N / (p/c)
		sparse->rows = sparse->N / (num_processes / repl_fact);
		//w tym konkteście to będą columnIndices		
		sparse->IA_row_offsets = malloc(sizeof(int) * sparse->rows * sparse->N );

		int rowsToReceive = sparse->rows / repl_fact; ///c część paska replikowanego w grupie
		MPI_Status status;
		MPI_Recv(
			sparse->IA_row_offsets,
			rowsToReceive+1,
			MPI_INT,
			0,
			COL_A_SPARSE_SEND_IA,
			MPI_COMM_WORLD,
			&status);
	
		int elements_to_receive = sparse->IA_row_offsets[rowsToReceive] - sparse->IA_row_offsets[0];
		sparse->elements = malloc(sizeof(double) * elements_to_receive);
			
		MPI_Recv(
			sparse->elements,
			elements_to_receive,
			MPI_DOUBLE,
			0,
			COL_A_SPARSE_SEND_ELEMENTS,
			MPI_COMM_WORLD,
			&status);
 
		sparse->JA_column_indices = malloc(sizeof(double) * elements_to_receive);
		MPI_Recv(
			sparse->JA_column_indices,
			elements_to_receive,
			MPI_INT,
			0,
			COL_A_SPARSE_SEND_JA,
			MPI_COMM_WORLD,
			&status);
	
	//zebranie cześci macierzy A przez przedstawicieli c-elementowych grup
	gather_sparse_Col_A(sparse,  mpi_rank, num_processes, repl_fact, group_comm, rank_in_group);
 }
 
 
 /*****************************scatter sparse Col A*********************************************************************/
 
 void scatter_sparse(int num_processes, int repl_fact, sparse_type sparse, int *columnsOffsets, MPI_Comm *group_comm, int * rank_in_group){

	int *tmpColOffs = malloc(sizeof(int) * sparse->N + 1);
	int sum = 0, pom;
	for(int i = 0; i < sparse->N+1; ++i){
		tmpColOffs[i] = 0;
		pom = columnsOffsets[i];
		columnsOffsets[i] = sum;
		sum += pom;
	}

	int elementsNum = (sparse->IA_row_offsets[sparse->N] - sparse->IA_row_offsets[0]);
	double * elementsColumned = malloc(sizeof(double) * elementsNum);
	int * rowsIndices = malloc(sizeof(int) * elementsNum);
	int currentRow = 0, seenElements = sparse->IA_row_offsets[0];
	for(int i = 0; i < elementsNum; ++i){
		while(seenElements >= sparse->IA_row_offsets[currentRow + 1])
					currentRow++;
		elementsColumned[columnsOffsets[sparse->JA_column_indices[i]] + tmpColOffs[sparse->JA_column_indices[i]]] = sparse->elements[i];
		seenElements ++;
		rowsIndices[columnsOffsets[sparse->JA_column_indices[i]] + tmpColOffs[sparse->JA_column_indices[i]]++] = currentRow;
	}

		//zostawiam sobie te elementy
	int sent_elements = columnsOffsets[myLast(sparse->N, 0, num_processes) + 1] - columnsOffsets[myFirst(sparse->N, 0, num_processes)];
	int elements_to_send = 0;
	
	sparse-> IA_row_offsets = columnsOffsets;
	sparse->elements = elementsColumned;
	sparse->JA_column_indices = rowsIndices;
	
	for(int adresat = 1; adresat < num_processes; ++adresat){
		MPI_Send(
			columnsOffsets + myFirst(sparse->N, adresat, num_processes),
			myLast(sparse->N, adresat, num_processes) - myFirst(sparse->N, adresat, num_processes) + 2, //kawałek dł n/p + 1  
			MPI_INT,
			adresat,
			COL_A_SPARSE_SEND_IA,
			MPI_COMM_WORLD);
		//teraz adresat wie, ile niezerowych elementów dostanie ->tyle musi sobie zaalokować przestrzeni

		elements_to_send = columnsOffsets[myLast(sparse->N, adresat, num_processes) + 1] - columnsOffsets[myFirst(sparse->N, adresat, num_processes)];

		MPI_Send(
			elementsColumned + sent_elements,
			elements_to_send,
			MPI_DOUBLE,
			adresat,
			COL_A_SPARSE_SEND_ELEMENTS,
			MPI_COMM_WORLD);

		MPI_Send(
			rowsIndices + sent_elements,
			elements_to_send,
			MPI_INT,
			adresat,
			COL_A_SPARSE_SEND_JA,
			MPI_COMM_WORLD);
		
		sent_elements += elements_to_send;
	}
	sparse->rows = sparse->N / (num_processes / repl_fact);
	gather_sparse_Col_A(sparse,  0, num_processes, repl_fact, group_comm, rank_in_group);
}
 
 /**************************************************************************************************/

 /*****************************FUNCTIONS FOR INNER ABC*********************************************************************/
 
 /****************************MAIN FUNCTION TO MULTIPLY********************************************************************/
 double* innerABC(sparse_type sparse, int num_processes, int repl_fact, int mpi_rank, double *B, MPI_Comm *group_comm, int rank_in_group, int exponent){

	//pasek kolumn macierzy b = nr_grupy
	//pasek wierszy macierzy A - nr_grupy + mpi_rank - nr_grupy * repl_fact // numer grupy + numer w grupie
	
	//komunikator do komunikacji w grupie jest stworzony wcześniej

	double * totalC = B;
	double * C = malloc(sizeof(double) * sparse->N * sparse->rows);
	MPI_Request IARequest, JARequest, ElementsRequest;
	MPI_Status myStatus;
	int * toFreeIA = NULL;
	int * toFreeJa= NULL;
	double * toFreeElems = NULL;
	
	//Po kolejntych mnozeniach macierz wynikowa będzie się nam przewijała, początek może być w środku. ->wynika ze sposobu składania C w allgatherV
	int Bstart = 0; 

	//do przerobienia jest N wierszy.
	//w każdej rundzie przerabiamy P wierszy
	              
	int numRounds = ile_iteracji(num_processes, repl_fact);

	for(int expIter = 0; expIter < exponent; ++ expIter){
		for(int row = 0; row < sparse->N; ++row){
			for(int col = 0; col < sparse->rows; ++col)
				C[row * sparse->rows + col] = 0;
			}
			
			for(int round = 0; round < numRounds; ++round){
	
			//na koniec poprzedniej rundy dostaję nowe sparse -> shift w lewo
				int elementsNum = sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0];
				int row = 0;
				int seenElements = sparse->IA_row_offsets[row]; //wiemy, ile elementów, których nie mamy, było wczesniej.
	
				//wiersze policzone w poprzednich rundach round * repl_fact * sparse->rows przez procesy "starsze" w grupie
				//+ to, co policzymy w tej rundzie  sparse->rows
				if((repl_fact - rank_in_group -1) * numRounds * sparse->rows +  (round +1 )* sparse->rows <= sparse->N){ 
					for(int nnzIndex = 0; nnzIndex < elementsNum; ++nnzIndex){
						while(seenElements >= sparse->IA_row_offsets[row + 1])
							row++;
						
						for(int internalColumn = 0; internalColumn < sparse->rows; ++internalColumn){
							C[(round * sparse->rows * sparse->rows) + (sparse->rows *row) +internalColumn] 
							//Bstart * sparseRows - wskazuje na miejsce w macierzy b, gdzie jest jej początek
							+= B[((Bstart  + sparse->JA_column_indices[nnzIndex]) % sparse-> N) * sparse->rows + internalColumn] * sparse->elements[nnzIndex];
						}
						seenElements++;
					}
				}
	
				//przesunięcie macierzy sparse między procesami - dogadujemy się z grupą odległą o 1
	
				if(toFreeIA != NULL){
					MPI_Wait(&IARequest, &myStatus);
					free(toFreeIA);
					toFreeIA = NULL;
					MPI_Wait(&ElementsRequest, &myStatus);
					free(toFreeElems);
					toFreeElems = NULL;
					MPI_Wait(&JARequest, &myStatus);
					free(toFreeJa);
					toFreeJa = NULL;
				}
	
				if(round < numRounds - 1){ //nie warto przesyłać po ostatniej rundzie
					MPI_Isend(
						sparse->IA_row_offsets,
						sparse->rows + 1,
						MPI_INT, 
						lewy_sasiad(mpi_rank, repl_fact, num_processes), //do odpowiedniego procesu 1 grup dalej
						SHIFT_IA,
						MPI_COMM_WORLD, 
						&IARequest);
					
					MPI_Isend(
						sparse->elements,
						sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
						MPI_DOUBLE, 
						lewy_sasiad(mpi_rank, repl_fact, num_processes), //do odpowiedniego procesu o 1 w lewo
						SHIFT_ELEMENTS,
						MPI_COMM_WORLD, 
						&ElementsRequest);
						
					MPI_Isend(
						sparse->JA_column_indices,
						sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
						MPI_INT, 
						lewy_sasiad(mpi_rank, repl_fact, num_processes), //do odpowiedniego procesu c grup dalej
						SHIFT_JA,
						MPI_COMM_WORLD, 
						&JARequest);
				
					int* buf = malloc(sizeof(int) * sparse->rows );
					MPI_Recv(
						buf,
						sparse->rows + 1,
						MPI_INT,
						prawy_sasiad(mpi_rank, repl_fact, num_processes),
						SHIFT_IA,
						MPI_COMM_WORLD,
						&myStatus);
						
					//przepinam bufor, który może jeszze się wysyła asynchronicznie na zmienną do usunięcia w kolejnym obrocie
					toFreeIA = sparse->IA_row_offsets;
					sparse->IA_row_offsets = buf;
					
					int *JAbuf = malloc(sizeof(int) * sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0]);
					double* tmpElements =  malloc(sizeof(double) * sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0]);
					
					MPI_Recv(
						tmpElements,
						sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
						MPI_DOUBLE,
						prawy_sasiad(mpi_rank, repl_fact, num_processes),
						SHIFT_ELEMENTS,
						MPI_COMM_WORLD,
						&myStatus);
						
					MPI_Recv(
						JAbuf,
						sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
						MPI_INT,
						prawy_sasiad(mpi_rank, repl_fact, num_processes),
						SHIFT_JA,
						MPI_COMM_WORLD,
						&myStatus);
						
					toFreeElems = sparse->elements;
					sparse->elements =tmpElements;
					toFreeJa = sparse->JA_column_indices;
					sparse->JA_column_indices = JAbuf;
				}
				else if((expIter < exponent - 1) && (numRounds > 1) ){ //po ostatniej rundzie w ramach rounds, kiedy szykuje się jeszcze jedna kolejka potęgowania, wróć z danymi do pocztku				
					MPI_Isend(
						sparse->IA_row_offsets,
						sparse->rows + 1,
						MPI_INT, 
						prawy_sasiad_koniec_rundy(mpi_rank, repl_fact, num_processes), 
						SHIFT_IA, 
						MPI_COMM_WORLD, 
						&IARequest);
						
					MPI_Isend(
						sparse->elements,
						sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
						MPI_DOUBLE, 
						prawy_sasiad_koniec_rundy(mpi_rank, repl_fact, num_processes), //do odpowiedniego procesu o 1 w lewo
						SHIFT_ELEMENTS,
						MPI_COMM_WORLD, 
						&ElementsRequest);
						
					MPI_Isend(
						sparse->JA_column_indices,
						sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
						MPI_INT, 
						prawy_sasiad_koniec_rundy(mpi_rank, repl_fact, num_processes), //do odpowiedniego procesu c grup dalej
						SHIFT_JA,
						MPI_COMM_WORLD, 
						&JARequest);
				
					int* buf = malloc(sizeof(int) * sparse->rows );
					
					MPI_Recv(
						buf,
						sparse->rows + 1,
						MPI_INT,
						lewy_sasiad_koniec_rundy(mpi_rank, repl_fact, num_processes),
						SHIFT_IA,
						MPI_COMM_WORLD,
						&myStatus);
						
					//przepinam bufor, który może jeszze się wysyła asynchronicznie na zmienną do usunięcia w kolejnym obrocie
					toFreeIA = sparse->IA_row_offsets;
					sparse->IA_row_offsets = buf;
					
					int *JAbuf = malloc(sizeof(int) * sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0]);
					double* tmpElements =  malloc(sizeof(double) * sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0]);
					
					MPI_Recv(
						tmpElements,
						sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
						MPI_DOUBLE,
						lewy_sasiad_koniec_rundy(mpi_rank, repl_fact, num_processes),
						SHIFT_ELEMENTS,
						MPI_COMM_WORLD,
						&myStatus);
						
					MPI_Recv(
						JAbuf,
						sparse->IA_row_offsets[sparse->rows] - sparse->IA_row_offsets[0],
						MPI_INT,
						lewy_sasiad_koniec_rundy(mpi_rank, repl_fact, num_processes),
						SHIFT_JA,
						MPI_COMM_WORLD,
						&myStatus);
					
					toFreeElems = sparse->elements;
					sparse->elements =tmpElements;
					toFreeJa = sparse->JA_column_indices;
					sparse->JA_column_indices = JAbuf;
				}
				
			}
			if(toFreeIA != NULL){
					MPI_Wait(&IARequest, &myStatus);
					free(toFreeIA);
					toFreeIA = NULL;
					MPI_Wait(&ElementsRequest, &myStatus);
					free(toFreeElems);
					toFreeElems = NULL;
					MPI_Wait(&JARequest, &myStatus);
					free(toFreeJa);
					toFreeJa = NULL;
			}
			
			//zebranie wyników C w ramach grup w jedną całość
			//liczba policzonych kawałków

			if(repl_fact > 1){
				totalC = malloc(sizeof(double) * sparse->N * sparse->rows);
				int * rcounts, *displs;
				rcounts = malloc(sizeof(int)* repl_fact);
				displs = malloc(sizeof(int)* repl_fact);

				int totalDispl = 0;
				for(int i = 0; i < repl_fact; ++i){
					//poniższy if -> ile wierszy moze przerobić konkretny proces
					if((repl_fact - i) * numRounds * sparse->rows <= sparse->N){ 
						rcounts[i] = numRounds * sparse->rows * sparse->rows;
					}
					else
						rcounts[i] = max(0, (sparse->N - (repl_fact - i-1) * numRounds * sparse->rows)) * sparse->rows;
					displs[i] = totalDispl;
					totalDispl += rcounts[i];
				}
				
				MPI_Allgatherv(
					C,  
					rcounts[rank_in_group],
					MPI_DOUBLE,
					totalC,
					rcounts,
					displs, 
					MPI_DOUBLE, 
					*group_comm);
					
				free(B);
				B = totalC;
			}
			else{
				free(B);
				B = C;
				totalC = B;
				C =  malloc(sizeof(double) * sparse->N * sparse->rows);
			}
			
			//przesunięcie B (N - numer grupy mod N) + (num) - zmienia się tylko po pierwsyzm obrocie, potem ejt już takie samo
			//N + wiersz, od którego zaczął wpisywać ostatni z grupy, kierownik
			//group_num - numer kawałka, liczonego w pierwszej rundzie przez kierownika pionowej grupy
			//N - aby opuścić modulo. N- group_size - aby wiedzieć w którym wierszu jest to wpisane.
			Bstart = modulo((sparse->N - numRounds * sparse->rows ) -  my_group_number( mpi_rank, repl_fact) * sparse->rows  ,sparse->N);	
		}

		double *shiftedResult = NULL;
		if(rank_in_group == 0 ){
			if(Bstart == 0){
				shiftedResult = totalC;
			}
			else{
				shiftedResult = malloc(sizeof(double) * sparse->N * sparse->rows);
				//przewiń na koniec macierz, aby bstart == 0
				//niech to robi tylko przedstawiciel grupy
				for(int r = 0; r < sparse->N; ++r){
					for(int i = 0; i < sparse->rows; ++i){
						shiftedResult[(modulo(r - Bstart, sparse->N)) * sparse->rows + i] = totalC[r * sparse->rows + i];
					}
				}
				free(totalC);
			}
		}
	
	return shiftedResult;
 }
 

 /*****************************CACHE DENSE*********************************************************************/
 
 double* cache_dense_inner(sparse_type sparse, int gen_seed, int repl_fact, int mpi_rank, int num_processes, 
   MPI_Comm *group_comm, int rank_in_group, int originalN){
	//najpierw sobie wygeneruj swoją część macierzy b a potem AllGather
	int first = myFirst(sparse->N, (mpi_rank /  repl_fact) * repl_fact, num_processes); //policz pierwszą kolumnę dla pierwszego w grupie
	double *dense = generate_dense_inner(
		sparse->N / repl_fact, 
		(sparse->N * repl_fact) / num_processes, 
		rank_in_group  * sparse->N / repl_fact, 
		first, 
		gen_seed, 
		originalN);
		
	if(repl_fact > 1){
		double *denseBuf = malloc(sizeof(double) * sparse->N *  sparse->rows);
		//allgather, każdy dostaje N /(P/c)
		MPI_Allgather( 
			dense, 
			sparse->N * sparse->N / num_processes, 
			MPI_DOUBLE, 
			denseBuf, 
			sparse->N * sparse->N / num_processes, 
			MPI_DOUBLE, 
			*group_comm); 
			
			
		free(dense);
		dense = denseBuf;
	}
	return dense;
 }
 
 
 /*****************************GATHER*********************************************************************/
  void gather_sparse(sparse_type sparse, int mpi_rank, int num_processes, int repl_fact, MPI_Comm *group_comm, int * rank_in_group){

	if(repl_fact > 1){ //c==1 -> kazdy ma swoje elementy
		int nr_grupy = mpi_rank / repl_fact;
		//pasek kolumn macierzy b = nr_grupy
		//pasek wierszy macierzy A - nr_grupy + mpi_rank - nr_grupy * repl_fact // numer grupy + numer w grupie

		//tworzenie komunikatora na potrzeby grupy
		MPI_Comm_split(
            MPI_COMM_WORLD,
            nr_grupy,
            my_rank_in_group(mpi_rank, repl_fact),
            group_comm);

		int group_size;
		MPI_Comm_size(*group_comm, &group_size);
        MPI_Comm_rank(*group_comm, rank_in_group);
		
		//gather - zbierz dane z grupy do przedstawiciela - rank n-1
		int rowsToSend = myLast(sparse->N, mpi_rank, num_processes) - myFirst(sparse->N, mpi_rank, num_processes) + 1;
		int * resbuf = NULL;
		int kierownik_grupy = repl_fact - 1;;//zbiera ostatni z grupy
		int kierownik_diagonali = repl_fact - 1;
		if((*rank_in_group) == kierownik_grupy)
			resbuf = malloc(sizeof(int) * rowsToSend * group_size + 1);  //+1 na potrzeby ostatniego elementu IA
		
	   //zebranie IA
		MPI_Gather(
			sparse->IA_row_offsets,
			rowsToSend,
			MPI_INT,
			resbuf,
			rowsToSend,
			MPI_INT,
			kierownik_grupy, 
			*group_comm
		);
		
		//przepisanie ostatniego elementu do nowego bufora, zwolnienie pamięci
		if((*rank_in_group) == kierownik_grupy){
			resbuf[rowsToSend * group_size] = sparse->IA_row_offsets[rowsToSend];
			free(sparse->IA_row_offsets);
			sparse->IA_row_offsets = resbuf;	
		}
		//kierownicy mają IA dla całej diagonalnej grupy
	
		int * nnzs = NULL, *displs = NULL, *JAelements = NULL;
		//policzenie NNz per proces do GatherV
		
		double * elementsBuf = NULL;;
		if((*rank_in_group) == kierownik_grupy){
			nnzs = malloc(sizeof(int)* group_size);
			displs = malloc(sizeof(int)* group_size);
			nnzs[0] = sparse->IA_row_offsets[rowsToSend] - sparse->IA_row_offsets[0];
			displs[0] = 0;
			for(int i = 1; i < group_size; ++i){
				nnzs[i] = sparse->IA_row_offsets[(i+1) * rowsToSend] - sparse->IA_row_offsets[i * rowsToSend];
				displs[i] = displs[i-1] + nnzs[i -1];
			}
			//bufor na wszystkie elementy nnz z danego paska wierszy macierzy A
			elementsBuf = malloc(sizeof(double) * (sparse->IA_row_offsets[group_size * rowsToSend] - sparse->IA_row_offsets[0]));
		}
	
		//zebranie elements
		int sendingElements = 0;
		if((*rank_in_group) == kierownik_grupy) //kierownik_grupy_grupy bierze już z właściwego miejsca w IA, bo ma całą.
			sendingElements =  sparse->IA_row_offsets[rowsToSend * group_size] - sparse->IA_row_offsets[rowsToSend * (group_size -1)];
		else
			sendingElements = sparse->IA_row_offsets[rowsToSend] - sparse->IA_row_offsets[0];
			
		MPI_Gatherv( 
			sparse->elements, 
			sendingElements, 
			MPI_DOUBLE, 
			elementsBuf, 
			nnzs, 
			displs, 
			MPI_DOUBLE, 
			kierownik_grupy, 
			*group_comm); 

		//przepisanie wszystkich elementów do bufora kierownika
		//jego część elements jest już w elementsBuf
		if((*rank_in_group) == kierownik_grupy){
			free(sparse->elements);
			sparse->elements = elementsBuf;
			JAelements = malloc(sizeof(int) * sparse->IA_row_offsets[group_size * rowsToSend] - sparse->IA_row_offsets[0]);
		}

		MPI_Gatherv( 
			sparse->JA_column_indices,
			sendingElements, 
			MPI_INT, 
			JAelements, 
			nnzs, 
			displs, 
			MPI_INT, 
			kierownik_grupy, 
			*group_comm); 

		//przepięcie bufora na JA kierownika
		if((*rank_in_group) == kierownik_grupy){
			free(sparse->JA_column_indices);
			sparse->JA_column_indices = JAelements;
		}

		//rozgłoszenie fragmentów sparse do zainteresowanych
		//tworzymy nowe komunikatory, "skośne" do rozgłoszenia "szerokich wierszy" macierzy A N/p/c
	
		//dzielmy tak, aby dany proces liczył kolejne wiersze C

		int diagonalGroup = diagonal_group_num( mpi_rank, *rank_in_group, repl_fact,  num_processes);
		int diagonal_group_size, diagonal_rank;
		MPI_Comm diagonal_comm;
		MPI_Comm_split(
            MPI_COMM_WORLD,
            diagonalGroup,
            my_rank_in_diagonal_group(mpi_rank, repl_fact),
            &diagonal_comm);
	
		MPI_Comm_size(diagonal_comm, &diagonal_group_size);
        MPI_Comm_rank(diagonal_comm, &diagonal_rank);
		
		//rozesłanie macierzy A do elementów diagonalnej grupy
		MPI_Bcast(
			sparse->IA_row_offsets,
			rowsToSend * group_size + 1,
			MPI_INT,
			kierownik_diagonali,
			diagonal_comm);
			
		//zaalokuj odpowiedniąprzestrzeń na nnz
		if(diagonal_rank != kierownik_diagonali){
			free(sparse->elements);
			free(sparse->JA_column_indices);
			sparse->elements = malloc(sizeof(double) * sparse->IA_row_offsets[group_size * rowsToSend] - sparse->IA_row_offsets[0]);
			sparse->JA_column_indices = malloc(sizeof(int) * sparse->IA_row_offsets[group_size * rowsToSend] - sparse->IA_row_offsets[0]);
		}

		//broadcast elementów niezerowych i columnindices
		MPI_Bcast(
			sparse->elements,
			sparse->IA_row_offsets[group_size * rowsToSend] - sparse->IA_row_offsets[0],
			MPI_DOUBLE,
			kierownik_diagonali,
			diagonal_comm);

		MPI_Bcast(
			sparse->JA_column_indices,
			sparse->IA_row_offsets[group_size * rowsToSend] - sparse->IA_row_offsets[0],
			MPI_INT,
			kierownik_diagonali,
			diagonal_comm);
	}
	else
	 (*rank_in_group) = 0;
 }
 
 
 /****************************CACHE SPARSE*************************************************************************/
 
  //odebranie od procesu 0 danych o macierzy rzadkiej
 void cache_sparse_inner(sparse_type sparse, int mpi_rank,int num_processes,int repl_fact, MPI_Comm *group_comm, int* rank_in_group ){

		//alokacja pamięci na IA: N / (p/c)
		sparse->rows = sparse->N / (num_processes / repl_fact);			
		sparse->IA_row_offsets = malloc(sizeof(int) * sparse->rows * sparse->N );
	
		int rowsToReceive = sparse->rows / repl_fact; ///c część paska replikowanego w grupie
		MPI_Status status;
		MPI_Recv(
			sparse->IA_row_offsets,
			rowsToReceive+1,
			MPI_INT,
			0,
			SPARSE_SEND_IA,
			MPI_COMM_WORLD,
			&status);
	
		int elements_to_receive = sparse->IA_row_offsets[rowsToReceive] - sparse->IA_row_offsets[0];
		sparse->elements = malloc(sizeof(double) * elements_to_receive);

		MPI_Recv(
			sparse->elements,
			elements_to_receive,
			MPI_DOUBLE,
			0,
			SPARSE_SEND_ELEMENTS,
			MPI_COMM_WORLD,
			&status);
 
		sparse->JA_column_indices = malloc(sizeof(int) * elements_to_receive);

		MPI_Recv(
			sparse->JA_column_indices,
			elements_to_receive,
			MPI_INT,
			0,
			SPARSE_SEND_JA,
			MPI_COMM_WORLD,
			&status);
					
	//zebranie cześci macierzy A przez przedstawicieli c-elementowych grup
	gather_sparse(sparse,  mpi_rank, num_processes, repl_fact, group_comm, rank_in_group);
 }
 
 
 /******************************SCATTER SPARSE*********************************************************************************************/
 
 void scatter_sparse_inner(int num_processes, sparse_type sparse,int repl_fact, MPI_Comm *group_comm, int* rank_in_group){
//proces 0 wysyła do każdego jego porcję - N/p wierszy
	MPI_Request *IARequests = malloc(sizeof(MPI_Request) * (num_processes - 1));
	MPI_Request *JARequests = malloc(sizeof(MPI_Request) * (num_processes - 1));
	MPI_Request *ElementsRequest = malloc(sizeof(MPI_Request) * (num_processes - 1));
    MPI_Status *statusses = malloc(sizeof(MPI_Status) * (num_processes - 1) * 3);
	
	//zostawiam sobie te elementy
	int sent_elements = sparse->IA_row_offsets[myLast(sparse->N, 0, num_processes) + 1] - sparse->IA_row_offsets[myFirst(sparse->N, 0, num_processes)];
	int elements_to_send = 0;
	for(int adresat = 1; adresat < num_processes; ++adresat){
			MPI_Isend(
			sparse->IA_row_offsets + myFirst(sparse->N, adresat, num_processes),
			myLast(sparse->N, adresat, num_processes) - myFirst(sparse->N, adresat, num_processes) + 2, //kawałek dł n/p + 1  
			MPI_INT,
			adresat,
			SPARSE_SEND_IA,
			MPI_COMM_WORLD,
			IARequests + adresat - 1);
			//teraz proces wie, ile niezerowych elementów dostanie ->tyle musi sobie zaalokować przestrzeni

			elements_to_send = sparse->IA_row_offsets[myLast(sparse->N, adresat, num_processes) + 1] - sparse->IA_row_offsets[myFirst(sparse->N, adresat, num_processes)];
			MPI_Isend(
			sparse->elements + sent_elements,
			elements_to_send,
			MPI_DOUBLE,
			adresat,
			SPARSE_SEND_ELEMENTS,
			MPI_COMM_WORLD,
			JARequests + adresat - 1);
			
			MPI_Isend(
			sparse->JA_column_indices + sent_elements,
			elements_to_send,
			MPI_INT,
			adresat,
			SPARSE_SEND_JA,
			MPI_COMM_WORLD,
			ElementsRequest + adresat - 1);
			
			sent_elements += elements_to_send;
	}
	
	//czekam na odebranie komunikatów, aby bezpiecznie uzyć tych struktur
	MPI_Waitall(num_processes - 1, IARequests, statusses);
	MPI_Waitall(num_processes - 1, JARequests, statusses + num_processes - 1);
	MPI_Waitall(num_processes - 1, ElementsRequest, statusses + 2*(num_processes - 1));
	
	
	sparse->rows = sparse->N / (num_processes / repl_fact);
	gather_sparse(sparse,  0, num_processes, repl_fact, group_comm, rank_in_group);
  }
 
 
 
 /****************************************************************************************************/
 
 /***********************************READING SPARSE *************************************************/
 
 sparse_type read_sparse(char* filePath, int num_processes, int *originalRows, int ** columnsOffset){
 
	FILE *fr;
	sparse_type result = malloc(sizeof(*result));
 
	int nnz = 0;
    if((fr = fopen(filePath, "rt")) != NULL){
	
	//WCZYTANIE N NNZ...
	
		if(fscanf(fr, "%d %d %d %d\n", &(result->rows),&(result->columns),&(nnz),&(result->maxNonZeroPerRow))){
			//dopełnienie zerami
			(*originalRows) = result->rows;
			result->rows = (result->rows / num_processes + (result->rows % num_processes > 0 ? 1:0)) * num_processes;
			result->columns = (*originalRows);
			result->N = result->rows;
			result->NNZ = nnz; //w zasadzie niepotrzebne, do wyliczenia z IA, do usunięcia
			
			result->elements = malloc(sizeof(double) * nnz);
			result->IA_row_offsets = malloc(sizeof(int) * (result->N + 1));
			result->JA_column_indices = malloc(sizeof(int) * (nnz));
			
			//na potrzeby ColA
			(*columnsOffset) = malloc(sizeof(int) * result->N + 1);
			
	//WCZYTANIE ELEMENTS		
			for(int i = 0; i < nnz; ++i)
				if(fscanf(fr, "%lf", &(result->elements[i]))){
				}

	//WCZYTANIE OFFSETS
			for(int i = 0; i < result->N + 1; ++i)
				if(i <= (*originalRows)){
					if(fscanf(fr, "%d", &(result->IA_row_offsets[i]))){
					}
				}
				else
					result->IA_row_offsets[i] = result->IA_row_offsets[i - 1]; //dopełnianie zerami
				
	//WCZYTANIE WSKAZNIKOW KOLUMN
	
			for(int i = 0; i < nnz; ++i)
				if(fscanf(fr, "%d", &(result->JA_column_indices[i]))){
					(*columnsOffset)[result->JA_column_indices[i]]++;
				}
		}
		else{
		//error niezgodny ze specyfikacją format wejścia
		}
		fclose(fr);  
		
		return result;
	}
	return NULL;
 
 }
 /****************************************************************************************************/
 
int main(int argc, char * argv[])
{
  int show_results = 0;
  int use_inner = 0;
  int gen_seed = -1;
  int repl_fact = 1;

  int option = -1;

  double comm_start = 0, comm_end = 0, comp_start = 0, comp_end = 0;
  int num_processes = 1;
  int mpi_rank = 0, group_rank = 0;
  int exponent = 1;
  double ge_element = 0;
  int count_ge = 0;

  int originalN = 0;
  int *columnsOffsets = NULL; //na potrzeby ColA
  sparse_type sparse = NULL;

  MPI_Comm group_comm;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &num_processes);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  
	while ((option = getopt(argc, argv, "vis:f:c:e:g:")) != -1) {
		switch (option) {
		case 'v': show_results = 1; 
		break;
		case 'i': use_inner = 1;
		break;
		case 'f': if ((mpi_rank) == 0) 
		{ 
			if((sparse = read_sparse(optarg, num_processes, &originalN, &columnsOffsets)) == NULL){
				fprintf(stderr, "error reading sparse file");
				MPI_Finalize();
				return 3;
			}
		}
		else
				sparse = malloc(sizeof(*sparse));
		break;
		case 'c': repl_fact = atoi(optarg);
		break;
		case 's': gen_seed = atoi(optarg);
		break;
		case 'e': exponent = atoi(optarg);
		break;
		case 'g': count_ge = 1; 
		ge_element = atof(optarg);
		break;
		default: fprintf(stderr, "error parsing argument %c exiting\n", option);
		MPI_Finalize();
		return 3;
		}
	}
	if ((gen_seed == -1) || ((mpi_rank == 0) && (sparse == NULL)))
	{
		fprintf(stderr, "error: missing seed or sparse matrix file; exiting\n");
		MPI_Finalize();
		return 3;
	}

	comm_start =  MPI_Wtime();
	//Definiuję typ sparse-mpi
	
	MPI_Aint sparse_address;

	const int size = 3;
	MPI_Aint array_of_block_lengths[size];
	int block_array[size];
	MPI_Aint array_of_displacements[size];
	MPI_Datatype array_of_types[size];
	array_of_types[0]=MPI_INT;
	array_of_types[1]=MPI_INT;
	array_of_types[2]=MPI_INT;
	
	MPI_Get_address(&(*sparse), &sparse_address);
	MPI_Get_address(&(sparse->rows), array_of_block_lengths);
	MPI_Get_address(&(sparse->columns), array_of_block_lengths + 1);
	MPI_Get_address(&(sparse->N), array_of_block_lengths + 2);

	array_of_displacements[0] = offsetof( sparse_ref, rows );
	array_of_displacements[1] = offsetof( sparse_ref, columns );
	array_of_displacements[2] = offsetof( sparse_ref, N );
	
	block_array [0] = 1;
	block_array [1] = 1;
	block_array [2] = 1;
	
	MPI_Datatype mpi_sparse_type;
	MPI_Type_create_struct(size, block_array, array_of_displacements, 
                       array_of_types, &mpi_sparse_type);

	MPI_Type_commit(&mpi_sparse_type);

	
	//prześlij dane podstawowe - wymiary itp
	MPI_Bcast(
		sparse,
		1,
		mpi_sparse_type,
		0,
		MPI_COMM_WORLD);
		
	originalN = sparse->columns;
	if ((mpi_rank) == 0) {
			if(use_inner == 1)
				scatter_sparse_inner(num_processes, sparse, repl_fact, &group_comm, &group_rank);
			else
				scatter_sparse(num_processes, repl_fact, sparse,columnsOffsets,&group_comm, &group_rank);
	}
	else{
		if(use_inner == 1)
			cache_sparse_inner(sparse, mpi_rank, num_processes, repl_fact, &group_comm, &group_rank);
		else
			cache_sparse(sparse, mpi_rank, num_processes,  repl_fact, &group_comm, &group_rank);
	}
	
	//generuj swoje elementy macierzy B
	//macierz wypisywana kolumnami
	
	double* matrix;
	if(use_inner == 1)
		matrix = cache_dense_inner(sparse, gen_seed, repl_fact, mpi_rank, num_processes, &group_comm, group_rank, originalN);
	else
		matrix = cache_dense(sparse, gen_seed, repl_fact, mpi_rank, num_processes, &group_comm, originalN);

	MPI_Barrier(MPI_COMM_WORLD);
	comm_end = MPI_Wtime();
	comp_start = MPI_Wtime();
	
	double * multiplicationResult;
	double *rbuf = NULL;
	
	if((mpi_rank) == 0){
		rbuf = malloc(sizeof(double) * sparse->N * sparse->N);
	}
	
	if(use_inner == 1){
		multiplicationResult = innerABC(sparse, num_processes, repl_fact, mpi_rank, matrix, &group_comm, group_rank, exponent);
		//przedstawiciele grup mają zebrane macierze C z grupy, tutaj 0 zbierze wyniki
		int colour = MPI_UNDEFINED;
		if(mpi_rank % repl_fact == 0)
			colour = 0;
			
		MPI_Comm results_comm;
		MPI_Comm_split(
			MPI_COMM_WORLD,
			colour,
			mpi_rank,
			&results_comm);
		
		if(mpi_rank % repl_fact == 0){
			MPI_Gather(
				multiplicationResult,
				(sparse-> rows) * sparse->N,
				MPI_DOUBLE,
				rbuf,
				(sparse-> rows) * sparse->N,
				MPI_DOUBLE,
				0,
				results_comm
			);
		}
	}
	else{
		 multiplicationResult = colA(sparse, num_processes, repl_fact, mpi_rank, matrix, exponent);

		MPI_Gather(
			 multiplicationResult,
			  sparse->N*sparse->N / num_processes,
			 MPI_DOUBLE,
			 rbuf,
			  sparse->N*sparse->N / num_processes,
			 MPI_DOUBLE,
			 0,
			 MPI_COMM_WORLD
		 );
	}
	MPI_Barrier(MPI_COMM_WORLD);
	comp_end = MPI_Wtime();
  
  
	if (show_results) {
		if((mpi_rank) == 0){
			int segment = 0;
			int curSegCol = 0;
			int segment_cols = (sparse->N * repl_fact) / num_processes;
			if(use_inner == 0){
				segment_cols /= repl_fact;
			}
			int segment_size = sparse->N * segment_cols;
			int segment_num = sparse->N / segment_cols;
			
			printf("%d %d\n", originalN, originalN);
			
			for(int rows = 0; rows < originalN; ++rows){
				for(int columns = 0; columns < sparse->N; ++columns){
					if(rows < originalN && (segment * segment_cols + curSegCol)< originalN)
						printf(" %lf", rbuf[segment * segment_size + rows * segment_cols + curSegCol]); 
					curSegCol++;
					segment += curSegCol / segment_cols;
					segment %= segment_num;
					curSegCol %= segment_cols;
				}
				printf("\n");
			}
		}
	}
	if (count_ge)
	{
		int elements = 0;
		if((mpi_rank) == 0){
			for(int rows = 0; rows < sparse->N; ++rows){
				for(int columns = 0; columns < sparse->N; ++columns){
					if(rbuf[columns * sparse->rows + rows] > ge_element)
						elements++;
				}
			}
			printf("%d\n", elements);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);
	if(1 == 0)
		printf("%lf %lf\n",comm_end - comm_start ,  comp_end - comp_start);

	MPI_Finalize();
	return 0;
}
